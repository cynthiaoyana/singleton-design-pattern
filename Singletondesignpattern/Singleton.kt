clase Singleton constructor privado () { 
    var sampleName: String? = "" 
    init { 
       "Esto es {$ this} singleton" .also (:: println) 
    } 
    Titular del objeto privado { 
        val INSTANCE = Singleton () 
    } 
    objeto complementario { 
        instancia val : Singleton by lazy {Holder.INSTANCE} 
    }

}